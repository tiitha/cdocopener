package ee.tiitha;

import org.openeid.cdoc4j.CDOCDecrypter;
import org.openeid.cdoc4j.exception.CDOCException;
import org.openeid.cdoc4j.token.Token;
import org.openeid.cdoc4j.token.pkcs12.PKCS12Token;
import org.openeid.cdoc4j.token.pkcs12.exception.PKCS12Exception;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class CDOCOpener {
    private final static String p12_container = "/path/to/keys.p12";
    private final static String p12_password  = "my_secret";

    public static void main(String args[]) throws PKCS12Exception, FileNotFoundException, CDOCException {

        if (args.length == 0) {
            System.out.println("CDOC name not defined!");
            System.out.println("Add filename as the first parameter\ne.g. './cdocopener secret.cdoc'.");
            System.exit(1);
        }

        String cdoc_file = args[0];

        Token token = new PKCS12Token(
                new FileInputStream(p12_container),
                p12_password
        );

        List<File> files = new ArrayList<>();

        files = new CDOCDecrypter()
                .withToken(token)
                .withCDOC(new File(cdoc_file))
                .decrypt(new File("."));

        for (File dataFile : files) {
            System.out.println("+ Extracted " + dataFile.getName());
        }
    }}
